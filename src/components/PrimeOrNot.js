import React from 'react';
import ErrorIcon from '@material-ui/icons/Error';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';

class PrimeOrNot extends React.Component {

  constructor(props) {
    super(props);
    this.state = { prime: props.prime, assertion : props.assertion };
  }

  isPrime(n){
    // TODO

    return false;
  }

  render() {
    return (
      <div class="row">
        <div class="col-md-4">
          {(this.isPrime(this.state.prime) == this.state.assertion) &&  (this.assertion == true) &&
            <h5><span>{this.state.prime}</span> is a prime number</h5>
          }
          {(this.isPrime(this.state.prime) == this.state.assertion) &&  (this.assertion != true) &&
            <h5><span>{this.state.prime}</span> is not a prime number</h5>
          }
          {(this.isPrime(this.state.prime) != this.state.assertion) &&
            <h5><span>{this.state.prime}</span> may be a prime number</h5>
          }
        </div>
        <div class="col-md-7">
          {(this.isPrime(this.state.prime) == this.state.assertion) && (this.assertion != true) &&
            <h5>its smallest factor greater than 1 is <span>{this.isPrime(this.state.prime)}</span></h5>
          }
          {(this.isPrime(this.state.prime) != this.state.assertion) &&
            <h5>factors???</h5>
          }
        </div>
        <div class="col-md-1">
          {(this.isPrime(this.state.prime) != this.state.assertion) &&
            <ErrorIcon/>
          }
          {(this.isPrime(this.state.prime) == this.state.assertion) &&
            <CheckCircleOutlineIcon/>
          }
        </div>
      </div>
    );
  }
}

export default PrimeOrNot;
