import React from 'react';
import PrimeOrNot from './PrimeOrNot.js'

class PrimeOrNotContainer extends React.Component {

  constructor(props) {
    super(props);
    this.state = { primes: [{prime: 2, assertion: true},
                            {prime : 4, assertion : 4}]
                 };
  }

  render() {
    return (
      <div>
        {this.state.primes.map(function(entry, index){
                    return <PrimeOrNot prime={entry.prime} assertion={entry.assertion}/>;
        })}
      </div>
    );
  }
}

export default PrimeOrNotContainer;
