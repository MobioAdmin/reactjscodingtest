import React from 'react';
import logo from './logo.svg';
import './App.css';
import Container from '@material-ui/core/Container'
import PrimeOrNotContainer from './components/PrimeOrNotContainer.js'

function App() {
  return (
    <div className="App">
      <Container>
        <div class="row">
          <h1>Thanks for taking the Mobio Interactive coding test</h1>
          <h3>Please complete the component PrimeOrNot and add more test cases to the two provided.</h3>
        </div>
        <PrimeOrNotContainer/>
      </Container>
    </div>
  );
}

export default App;
